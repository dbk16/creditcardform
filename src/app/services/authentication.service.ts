import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError, ObservableInput } from 'rxjs';
import { map,  catchError, retry } from 'rxjs/operators';


import { User } from '../models';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(
    private http: HttpClient
  ) {    
  }

  private handleError(error: HttpErrorResponse): ObservableInput<ErrorEvent> {
		console.log(error);
		if (error.error instanceof ErrorEvent) {
			// Handler network error
			console.log(error.message);
			retry(2);
		} else {
			console.log(error.status);
			return throwError(error.error);
		}
		return throwError(error.message);
	}

	

	// public login( body: any, options: any = {}): Promise<any> {
	// 	return this.http.post('http://localhost:3000/savecreditData', body, options).pipe(catchError(this.handleError)).toPromise();
	// }
  login(credentials) {
    return this.http.post<any>(`http://localhost:3000/savecreditData`, credentials)
      .pipe(map(user => {
        // store user details and JWT token in local storage
        return user;
      }));
  }


}



	

	


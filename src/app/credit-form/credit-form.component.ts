import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '../services';

@Component({
  selector: 'app-credit-form',
  templateUrl: './credit-form.component.html',
  styleUrls: ['./credit-form.component.css']
})
export class CreditFormComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  isShowAlert = true;
  returnUrl: string;
  error = '';


  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService
  ) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      amount: ['', [Validators.required]],
      nameOnCc: ['', Validators.required],
      ccNumber: ['', Validators.required,Validators.minLength(12),Validators.maxLength(16)],
      ccExpiration: ['', Validators.required],
      ccSecurityCode: ['', [Validators.required, Validators.minLength(3)]]
    });

  }

  // getter | https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/get
  get f() {
    return this.loginForm.controls;
  }

  closeAlert() {
    this.isShowAlert = false;
  }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;

    const credentials = {
      nameOnCc: this.f.nameOnCc.value,
      ccNumber: this.f.ccNumber.value,
      ccExpiration: this.f.ccExpiration.value,
      ccSecurityCode: this.f.ccSecurityCode.value,
      amount: this.f.amount.value
    };
    this.authenticationService.login(credentials)
      .pipe(first())
      .subscribe(
        user => {
          console.log(user);
          //this.router.navigate([this.returnUrl]);
        },
        error => {
          this.error = error;
          this.loading = false;
        }
      );
  }


}

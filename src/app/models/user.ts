export class User {
  
  amount: string;
  nameOnCc: string;
  ccNumber: string;
  ccExpiration: string;
  ccSecurityCode: string;
}

var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var creditData = require('./credit-model');

var mongoose = require('mongoose');

// DB connection
   mongoose.connect('mongodb://localhost:27017/creditcard', function(err) { 
  if (err) {
    console.log('connection error', err);
  } else {
    console.log('connection successful....');
  }
});

var app = express();
app.use(bodyParser.json({ limit: '500mb' }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));


app.all('/*', function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key,enctype,authorization');
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});
app.set('views', path.join(__dirname, 'views'));


app.post('/savecreditData', (req, res) => {
    var data = new creditData({
        amount: req.body.amount,
    ccExpiration: req.body.ccExpiration,
    ccNumber:req.body.ccNumber,
    ccSecurityCode:req.body.ccSecurityCode,
    nameOnCc:req.body.nameOnCc
   });
   data.save(function (err, post) {
       if (err) {
           res.json({ "message": "Something went wrong", "status": 204 });
       } else {
           res.json({
               "message": "credit card successfully",
               "status": 200
           });
       }
   });
});
module.exports = app;
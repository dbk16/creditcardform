var mongoose = require('mongoose');


var creditData = new mongoose.Schema({
    nameOnCc:{type:String,default:null},
    ccNumber:{type:String,default:null},
    ccExpiration:{type:String,default:null},
    ccSecurityCode:{type:String,default:null},
    amount:{type:String,default:null}
        
});

module.exports = mongoose.model('creditModel', creditData);
